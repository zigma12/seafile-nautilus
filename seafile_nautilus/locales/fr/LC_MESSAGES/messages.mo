��    %      D  5   l      @  (   A  �   j     U     s     �  �   �     d     v     �     �     �  ,   �     �          .  8   B     {     �     �     �     �  <   �     �  �        �     �     �     �  :   �       2     Y   J  X   �  W   �  '   U	  ?   }	  _  �	  4     �   R  &   >     e     z  �   �     �     �     �     �     �  (        +     F     b  H   |  &   �     �     �     �        G     *   b  �   �     ,     ?     _     g  Q   �     �  A   �  ]     b   }  U   �  (   6  F   _                                                                            #              "      $   	   %            !                                        
                         About which link would you like details? Are you sure you want to delete the {kind} link?

The link to « {short_name} » will be <b>immediately and permanently invalidated</b>.
You cannot undo this action.
If you create a new link, it will be different from the current one. Change share link permissions Create share link Create upload link Current permissions are: <b>{curr_perm}</b>.

Click on <b>« {curr_perm} »</b> to leave the link permissions unchanged.
Click on any other button to change permissions.
Permission changes can be undone. Delete share link Delete upload link Delete {kind} link ? Details Details about the internal link Details about the link to « {short_name} » Details about the share link Details about the upload link Download and upload Each file/folder has such a link that cannot be deleted. Edit on cloud and download Error Field Local link: Login is required. Modification of share link permissions on « {short_name} » No login is required. One part of seafile local configuration data is missing:
{file}

Are you sure Seafile is correctly installed for the current user? Preview Only Preview and download Quit Share « {file_name} » The script takes exactly one file/folder path as argument. Value You must select <b>exactly one</b> file or folder. any user who has access to the links can access the files or folders pointed by the link. any user who has access to the links can upload files to the folder pointed by the link. only logged in users who have share permissions to file or folder can access this link. « {file} »
is not valid a valid path. « {href_file} »

does not belong to any synchronized library. Project-Id-Version: unnamed project
PO-Revision-Date: 2022-08-07 21:01+0200
Last-Translator: vinraspa <raspal@ntymail.com>
Language-Team: French <->
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
Generated-By: pygettext.py 1.5
X-Generator: Poedit 3.1.1
 À propos de quel lien souhaitez-vous des détails ? Confirmez-vous la suppression du {kind}-link?

Le lien vers « {short_name} » sera <b>immédiatement et définitivement invalidé</b>
sans retour en arrière possible.
Si vous créez à nouveau un lien, il sera différent de l'actuel. Modifier les permissions du share-link Créer un share-link Créer un upload-link Actuellement, les permissions sont : <b>{curr_perm}</b>.

Cliquer sur <b>« {curr_perm} »</b> pour laisser les permissions inchangées.
Cliquer sur un autre bouton pour ajuster les permissions.
Les changements que vous pourrez apporter sont réversibles. Supprimer le share-link Supprimer l'upload-link Supprimer le {kind}-link ? Détails Détails sur le internal-link Détails du lien vers « {short_name} » Détails sur le share-link Détails sur le upload-link Téléchargement et envoi Chaque fichier/dossier possède un tel lien qui ne peut être supprimé. Éditer dans le cloud et télécharger Erreur Champ Local: Authentification requise. Modification des permissions associées au lien vers « {short_name} » Aucune authentification n'est nécessaire. Un élément de configuration locale de Seafile est absent :
{file}

Êtes-vous certain que Seafile est installé et disponible pour l’utilisateur courant ? Aperçu uniquement Prévisualiser et télécharger Quitter Partage de « {file_name} » Vous devez passer en argument un et un seul chemin vers un fichier ou un dossier. Valeur Vous devez sélectionner <b>un et un seul</b> fichier ou dossier. quiconque possède ce lien pourra accéder aux fichiers ou aux dossiers pointés par ce lien. quiconque possède ce lien pourra téléverser des éléments vers le dossier pointé par ce lien. l'accès au fichier ou dossier est réservée aux personnes de la même organisation. « {file} »
n'est pas un chemin valide. « {href_file} »

n'appartient à aucune bibliothèque synchronisée. 